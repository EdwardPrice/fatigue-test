//
//  PauseView.swift
//  Reactions
//
//  Created by Edward Price on 10/02/2015.
//  Copyright (c) 2015 Edward Price. All rights reserved.
//

import Foundation
import UIKit
import QuartzCore

let pause_VIEW_ALPHA:CGFloat = 0.5
let pause_TIMERVIEW_RADIUS:CGFloat = 50
let pause_BORDER_WIDTH:CGFloat = 2
/* add custom button */
//let myFirstButton = UIButton()


class PauseView :UIView {
    
    struct Stored {
        static var timerLbl:UILabel!
    }
    
    
    class func loadingPauseViewInView (_superView:UIView)-> PauseView
    {
        let pauseView:PauseView = PauseView(frame:_superView.frame)
        pauseView.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(pause_VIEW_ALPHA)
        pauseView.layer.zPosition = 1;
        _superView.addSubview(pauseView)
        
        /* add a custom Circle view */
        
        let refFrame:CGRect = CGRectMake(_superView.center.x-pause_TIMERVIEW_RADIUS, _superView.center.y-pause_TIMERVIEW_RADIUS, 2*pause_TIMERVIEW_RADIUS, 2*pause_TIMERVIEW_RADIUS)
        let circleView:UIView = UIView(frame:refFrame)
        circleView.layer.cornerRadius = pause_TIMERVIEW_RADIUS
        circleView.layer.borderColor = UIColor.whiteColor().CGColor
        circleView.layer.borderWidth = BORDER_WIDTH
        
        var buttonCode : ()->()
        
        let myFirstButton = UIButton()
        myFirstButton.userInteractionEnabled = true
        //myFirstButton = UIButton(frame:circleView.bounds)
        myFirstButton.setTitle("Play", forState: .Normal)
        myFirstButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        myFirstButton.addTarget(self, action: "pressed:", forControlEvents: UIControlEvents.TouchUpInside)
        circleView.addSubview(myFirstButton)
        
        //circleView.addSubview(Stored.timerLbl)
        pauseView.addSubview(circleView)
        
        return pauseView
    }
    
    
    func pressed(sender: UIButton!) {
        print("");
        let mySuperView:UIView = self.superview!
        mySuperView.userInteractionEnabled = true
        super.removeFromSuperview()
    }
}