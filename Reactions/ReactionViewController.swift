//
//  ReactionViewController.swift
//  Reactions
//
//  Created by Edward Price on 16/02/2015.
//  Copyright (c) 2015 Edward Price. All rights reserved.
//

import UIKit

class ReactionViewController: UIViewController {
    
    @IBOutlet weak var backGround: UIImageView!
    @IBOutlet weak var background2: UIImageView!
    @IBOutlet weak var result: UILabel!
    //@IBOutlet weak var gameTimer: UILabel!
    @IBOutlet weak var gameTimer: UILabel!
    @IBOutlet weak var resultImage: UIImageView!
    
    
    //global reference of the start time of the reaction
    var startTime = NSTimeInterval()
    
    //two timers one for the 1sec delay before the random delay happens
    var timer = NSTimer()
    var delay = NSTimer()
    //timer for if the reaction time is longer than 4 secs
    var reactionSlow = NSTimer()
    var timerIsRunning = false

    //game timer variables
    var theGameTimer = NSTimer()
    var timerVal = 90
    
    var numberOfGames = 0
    var early = 0
    var correct = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        /* load a count down view from timerView */
        //self.view.userInteractionEnabled = false
        //var gamelaunchTimerView:TimerView = TimerView.loadingCountDownTimerViewInView(self.view)
        //gamelaunchTimerView.startTimer()
        //var timer = NSTimer.scheduledTimerWithTimeInterval(4.2, target: self, selector: Selector("startTimer"), userInfo: nil, repeats: false)

        
//        backGround.layer.borderWidth = 1.0
//        backGround.layer.borderColor = UIColor.blackColor().CGColor
//        background2.layer.borderWidth = 1.0
//        background2.layer.borderColor = UIColor.blackColor().CGColor
        
        let touch = UITapGestureRecognizer(target:self, action:"action")
        backGround.addGestureRecognizer(touch)
        
        var touch2 = UITapGestureRecognizer(target:self, action:"action")
        background2.addGestureRecognizer(touch)
        
    }
    
    
//    func startTimer()
//    {
//        theGameTimer = NSTimer.scheduledTimerWithTimeInterval(1.0
//            , target: self, selector: Selector("updateTimer:"), userInfo: nil, repeats: true)
//    }
//    
//    func updateTimer(dt:NSTimer)
//    {
//        timerVal--
//        if timerVal==0{
//            performSegueWithIdentifier("ReactionsSegue", sender: nil)
//        }else if timerVal<0{
//            theGameTimer.invalidate()
//        } else{
//            gameTimer.text = "\(timerVal)"
//        }
//    }
//        
//    func removeCountDownTimerView()
//    {
//        timerVal = 90
//        
//    }

    ////////////////////////////////////////////
    // Set background and get current saved data
    ///////////////////////////////////////////
    
    override func viewWillAppear(animated: Bool) {
        backGround.tag = 1
        background2.tag = 1
        
        super.viewWillAppear(animated)
        
    }
    ////////////////////////////////////////////
    // Set background and get current saved data
    ///////////////////////////////////////////
        
        
        
    
    /////////////////////////////
    // Touch event on the screen
    /////////////////////////////
    
    
//    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
//        
//        super.touchesBegan(touches, withEvent: event)
//        
//        
//        for touch: AnyObject in touches {
//            //run the tapped function
//            tapped()
//        }
//    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
        super.touchesBegan(touches, withEvent: event)
        
        for touch:AnyObject in touches {
            let touch = touches.first as? UITouch!
            let location = touch!.locationInView(self.view)
            
            let rect = backGround.frame
            let rect2 = background2.frame
            
            let top = "top"
            let bottom = "bottom"
            
            if CGRectContainsPoint(rect, location){
                print("tapped top")
                tapped(top)
            }
                
            else if CGRectContainsPoint(rect2, location){
                print("tapped bottom")
                tapped(bottom)
                
            }
            else {
            }
        }
    }
    /////////////////////////////
    // Touch event on the screen
    /////////////////////////////
        
        
        
        
    ///////////////////////////////////////////////////////////////////////////////
    // check what state the screen is in and do the appropiate action for the screen
    ///////////////////////////////////////////////////////////////////////////////
    
    var circleView = UIView()
    var circleView2 = UIView()
    
    func tapped(position: String) {
        
        if backGround.tag == 1{
            
            reactionSlow.invalidate()
            backGround.tag = 2
            background2.tag = 2
            backGround.backgroundColor = UIColor.whiteColor()
            background2.backgroundColor = UIColor.whiteColor()
            resultImage.alpha = 0
            result.text = "Wait..."
            delay = NSTimer.scheduledTimerWithTimeInterval(1.5, target: self, selector: Selector("randomShowPrompt"), userInfo: nil, repeats: false)
        }
            
        else if backGround.tag == 2{
            
            reactionSlow.invalidate()
            
            backGround.tag = 1
            background2.tag = 1
            result.text = "Too Soon!! \n Tap to start again"
            resultImage.alpha = 1
            resultImage.image = UIImage(named: "red-ex")
            backGround.backgroundColor = UIColor.whiteColor()
            background2.backgroundColor = UIColor.whiteColor()
            
            timer.invalidate()
            delay.invalidate()
            
            early++
            numberOfGames++
            
            if numberOfGames < 15{
                updateCounter()
            }
            else{
                scoreObject.earlyReactions = early
                scoreObject.correctReactions = correct
                performSegueWithIdentifier("ReactionsSegue", sender: nil)
            }
        }
            
        else if backGround.tag == 3{
            
            if (position == "top" && backGround.backgroundColor == UIColor(red: 80/255, green: 210/255, blue: 194/255, alpha: 1)) || (position == "bottom" && background2.backgroundColor == UIColor(red: 80/255, green: 210/255, blue: 194/255, alpha: 1)){
                
                let currentTime = NSDate.timeIntervalSinceReferenceDate()
                circleView.removeFromSuperview()
                circleView2.removeFromSuperview()
                
                reactionSlow.invalidate()
                
                backGround.backgroundColor = UIColor.whiteColor()
                background2.backgroundColor = UIColor.whiteColor()
                backGround.tag = 2
                background2.tag = 2
                
                //Find the difference between current time and start time.
                let elapsedTime: NSTimeInterval = currentTime - startTime
                
                
                let a:String = String(format:"%.2f", elapsedTime)
                result.text = "Reaction was: " + a + " seconds \n Get Ready..."
                resultImage.alpha = 1
                resultImage.image = UIImage(named: "green-tick")
                
                var date = NSDate()
                correct++
                
                
                saveTime(elapsedTime)
                numberOfGames++
                if numberOfGames < 15{
                    updateCounter()
                }
                else{
                    scoreObject.earlyReactions = early
                    scoreObject.correctReactions = correct
                    performSegueWithIdentifier("ReactionsSegue", sender: nil)
                }
                
                delay = NSTimer.scheduledTimerWithTimeInterval(2, target: self, selector: Selector("randomShowPrompt"), userInfo: nil, repeats: false)
                //saveName(elapsedTime, date: date)
                //saveToDB(elapsedTime, date: date)
            
            }
            
            else{
                reactionSlow.invalidate()
                
                backGround.tag = 1
                background2.tag = 1
                result.text = "Wrong Section \n Tap to start again"
                resultImage.alpha = 1
                resultImage.image = UIImage(named: "red-ex")
                
                circleView.removeFromSuperview()
                circleView2.removeFromSuperview()
                
                backGround.backgroundColor = UIColor.whiteColor()
                background2.backgroundColor = UIColor.whiteColor()
                
                timer.invalidate()
                delay.invalidate()
                
                early++
                numberOfGames++
                
                if numberOfGames < 15{
                    updateCounter()
                }
                else{
                    scoreObject.earlyReactions = early
                    scoreObject.correctReactions = correct
                    performSegueWithIdentifier("ReactionsSegue", sender: nil)
                }
            }
            
        }
        
    }
    /////////////////////////////
    // End of screen check state
    /////////////////////////////
        
    
    func updateCounter(){
        UIView.setAnimationsEnabled(false)
        gameTimer.text = "\(numberOfGames)/15"
        UIView.setAnimationsEnabled(true)
    }
        
        
    ///////////////////////
    //Start of random Timer
    ///////////////////////
    
    func randomShowPrompt(){
        // Seconds before performing next action. Choose a default value
        let timeUntilNextAction = CDouble(arc4random_uniform(5))
        
        timer = NSTimer.scheduledTimerWithTimeInterval(timeUntilNextAction, target: self, selector: Selector("showPrompt"), userInfo: nil, repeats: false)
        
    }
    
    
    func showPrompt() {
        result.text = " "
        resultImage.alpha = 0
        startTime = NSDate.timeIntervalSinceReferenceDate()
        
        let section = arc4random_uniform(2)
        
        
        if section == 0{
            backGround.backgroundColor = UIColor(red: 80/255, green: 210/255, blue: 194/255, alpha: 1)
            //background2.backgroundColor = UIColor.redColor()
            result.text = ""
            //instruction.font = UIFont(name: instruction.font.fontName, size: 28)
            backGround.tag = 3
            reactionSlow = NSTimer.scheduledTimerWithTimeInterval(4, target: self, selector: Selector("tooSlow"), userInfo: nil, repeats: false)
            /* add a custom Circle view */
            
            let superView = backGround
            
            let refFrame:CGRect = CGRectMake(superView.center.x-50, superView.center.y-15, 2*50, 2*50)
            circleView = UIView(frame:refFrame)
            circleView.layer.cornerRadius = 50
            circleView.layer.borderColor = UIColor(red: 80/255, green: 210/255, blue: 194/255, alpha: 1).CGColor
            circleView.layer.borderWidth = 3
            
            let myFirstLabel = UILabel()
            myFirstLabel.text = "Tap!"
            myFirstLabel.font = UIFont(name: myFirstLabel.font.fontName, size: 25)
            myFirstLabel.frame = circleView.bounds
            myFirstLabel.textColor = UIColor.whiteColor()
            myFirstLabel.textAlignment = .Center
            myFirstLabel.frame = circleView.bounds
            
            circleView.addSubview(myFirstLabel)
            backGround.addSubview(circleView)
            
        }
        
        if section == 1{
            //backGround.backgroundColor = UIColor.redColor()
            background2.backgroundColor = UIColor(red: 80/255, green: 210/255, blue: 194/255, alpha: 1)
            result.text = ""
            backGround.tag = 3
            reactionSlow = NSTimer.scheduledTimerWithTimeInterval(4, target: self, selector: Selector("tooSlow"), userInfo: nil, repeats: false)
            
            let superView = background2
            
            let refFrame:CGRect = CGRectMake(superView.center.x-50, superView.center.y-50, 2*50, 2*50)
            circleView2 = UIView(frame:refFrame)
            circleView2.layer.cornerRadius = 50
            circleView2.layer.borderColor = UIColor(red: 80/255, green: 210/255, blue: 194/255, alpha: 1).CGColor
            circleView2.layer.borderWidth = 3
            
            let myFirstLabel = UILabel()
            myFirstLabel.text = "Tap!"
            myFirstLabel.font = UIFont(name: myFirstLabel.font.fontName, size: 25)
            myFirstLabel.frame = circleView2.bounds
            myFirstLabel.textColor = UIColor.whiteColor()
            myFirstLabel.textAlignment = .Center
            myFirstLabel.frame = circleView2.bounds
            
            circleView2.addSubview(myFirstLabel)
            backGround.addSubview(circleView2)
        }
        
        
    }
    

    func tooSlow(){
        backGround.tag = 1
        backGround.backgroundColor = UIColor.whiteColor()
        background2.backgroundColor = UIColor.whiteColor()
        circleView.removeFromSuperview()
        circleView2.removeFromSuperview()
        
        resultImage.alpha = 1
        resultImage.image = UIImage(named: "red-ex")
        result.text = "No reaction detected \n Tap to start again"
        timer.invalidate()
        delay.invalidate()
    }
    /////////////////////
    //End of random timer
    /////////////////////
        
        
        
    ///////////////////////////
    //Save the data to core data
    ///////////////////////////
    
    func saveTime(time: Double) {
        
        let formattedTime = String(format:"%.2f", time)
        let formatConversion = (formattedTime as NSString).floatValue
        scoreObject.reactionTimesResults.append(formatConversion)
    }
    


        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
        
    }

