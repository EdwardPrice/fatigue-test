//
//  ViewController.swift
//  Reactions
//
//  Created by Edward Price on 19/01/2015.
//  Copyright (c) 2015 Edward Price. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var instructionLabel: UILabel!
    
    @IBOutlet weak var startTest: UIButton!
    
    @IBAction func start(sender: AnyObject) {
        timer.invalidate()
        performSegueWithIdentifier("startTestSegue", sender: nil)
    }
    
    var timer = NSTimer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        //this jobbo hides the navigation bar from all subviews starting here
        //self.navigationController?.navigationBar.hidden =  true
        
//        var localNotification:UILocalNotification = UILocalNotification()
//        localNotification.alertAction = "Fatigue Test"
//        localNotification.alertBody = "Have you taken the fatigue test today?"
//        localNotification.fireDate = NSDate(timeIntervalSinceNow: 30)
//        localNotification.repeatInterval = NSCalendarUnit.CalendarUnitDay
//        UIApplication.sharedApplication().scheduleLocalNotification(localNotification)
        
        
        
        timer = NSTimer.scheduledTimerWithTimeInterval(0.9, target: self, selector: Selector("checkTimes"), userInfo: nil, repeats: true)
        instructionLabel.text = ""
        startTest.enabled = false
        startTest.backgroundColor = UIColor.grayColor()
        startTest.alpha = 0.4
        
    }
    
    func checkTimes(){
        let defaults = NSUserDefaults.standardUserDefaults()
        if let time:NSDate = defaults.objectForKey("timeLastTaken") as? NSDate
        {
            let timeNow: NSDate = NSDate()
            let interval = timeNow.timeIntervalSinceDate(time as NSDate)
            print(interval)
            
            if interval > 3600 {
                startTest.alpha = 1
                startTest.enabled = true
                startTest.backgroundColor = UIColor(red: 80/255, green: 210/255, blue: 194/255, alpha: 1)
                instructionLabel.text = ""
            }
            else{
                let timeLeft = (3600 - interval)/60
                let formattedTime = NSString(format:"%.0f", timeLeft)
                instructionLabel.text = "Please wait \(formattedTime) minutes before taking the test again "
            }
        }
        else {
            startTest.alpha = 1
            startTest.enabled = true
            startTest.backgroundColor = UIColor(red: 80/255, green: 210/255, blue: 194/255, alpha: 1)
            instructionLabel.text = ""
        }
    }


    override func viewWillAppear(animated: Bool) {
        
        
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}


//////////////////////////////////
//Globally accessable scores
//////////////////////////////////

class Score {
    var spatialScore = Int()
    var spaticalNumberCompleted = Int()
    var spaticalNumberRight = Int()
    var spaticalNumberWrong = Int()
    var spaticalTimeToCompete = [Float]()
    
    var reactionTimesResults = [Float]()
    var correctReactions = Int()
    var earlyReactions = Int()
    
    var sumsRight = Int()
    var sumsWrong = Int()
    var correctAdditionAnswers = [Int]()
    var userAdditionAnswers = [Int]()
    var allAdditionQuestions = [String]()
    
    var questionaire = [Int]()
}

var scoreObject = Score()

//////////////////////////////////
//Globally accessable scores
//////////////////////////////////
