//
//  QuestionaireViewController.swift
//  Reactions
//
//  Created by Edward Price on 17/02/2015.
//  Copyright (c) 2015 Edward Price. All rights reserved.
//

import UIKit
import CoreData

class QuestionaireViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    

    @IBAction func submitResults(sender: AnyObject) {
//        scoreObject.questionaire.append(sliderOne.value)
//        scoreObject.questionaire.append(sliderTwo.value)
//        scoreObject.questionaire.append(sliderThree.value)
//        scoreObject.questionaire.append(sliderFour.value)
//        scoreObject.questionaire.append(sliderFive.value)
//        scoreObject.questionaire.append(sliderSix.value)
//        scoreObject.questionaire.append(sliderSeven.value)
//        scoreObject.questionaire.append(sliderEight.value)
//        scoreObject.questionaire.append(sliderNine.value)
//        scoreObject.questionaire.append(sliderTen.value)
//        scoreObject.questionaire.append(sliderEleven.value)
//        scoreObject.questionaire.append(sliderTwelve.value)
//        scoreObject.questionaire.append(sliderThirteen.value)
//        scoreObject.questionaire.append(sliderFourteen.value)
//        
//        
        saveToDB()
        //saveCoreData()
        //checkCoreDB()
        performSegueWithIdentifier("FinishSegue", sender: nil)
        //clearAllTestData()
    }
    
    ///////////////////////////
    //Save the data to core data
    ///////////////////////////
    
    var didItSend = Bool()
    
    func saveCoreData() {
        //1
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext!
        //2
        let entity =  NSEntityDescription.entityForName("TestResultsData", inManagedObjectContext: managedContext)
        let Results = NSManagedObject(entity: entity!, insertIntoManagedObjectContext:managedContext)
        //var error: NSError? = nil
        
        //var unique:NSManagedObjectID = managedContext.
            //obtainPermanentIDsForObjects(Results, error: &error)
        //3
        let theQuestionaire = scoreObject.questionaire
        let data = NSData(bytes: theQuestionaire, length: theQuestionaire.count * sizeof(Float))
        
        let theReactions = scoreObject.reactionTimesResults
        let data2 = NSData(bytes: theReactions, length: theReactions.count * sizeof(Float))
        let reactionsCorrect = scoreObject.correctReactions
        let reactionsEarly = scoreObject.earlyReactions

        let theSpatialComplete = scoreObject.spaticalNumberCompleted
        let theSpatialScore = scoreObject.spatialScore
        let theSpatialRight = scoreObject.spaticalNumberRight
        let theSpatialWrong = scoreObject.spaticalNumberWrong
        let theSpatialTimeToComplete = scoreObject.spaticalTimeToCompete
        let data3 = theSpatialTimeToComplete.description
        
        let theSumsRight = scoreObject.sumsRight
        let theSumsWrong = scoreObject.sumsWrong
        
        let allSumsQuestions = scoreObject.allAdditionQuestions
        let strRepresentation1 = allSumsQuestions.description
        print(strRepresentation1)
        
        let correctSumsAnswers = scoreObject.correctAdditionAnswers
        let strRepresentation2 = correctSumsAnswers.description
        print(strRepresentation2)
        
        let userSumsAnswers = scoreObject.userAdditionAnswers
        let strRepresentation3 = userSumsAnswers.description
        print(strRepresentation3)
        
        let date = NSDate()
    
        Results.setValue(data, forKey: "questionaire")
        
        Results.setValue(data2, forKey: "reactionTimesResults")
        Results.setValue(reactionsEarly, forKey: "earlyReactions")
        Results.setValue(reactionsCorrect, forKey: "correctReactions")
        
        Results.setValue(theSpatialComplete, forKey: "spatialNumberComplete")
        Results.setValue(theSpatialScore, forKey: "spatialScore")
        Results.setValue(theSpatialRight, forKey: "spaticalNumberRight")
        Results.setValue(theSpatialWrong, forKey: "spaticalNumberWrong")
        Results.setValue(data3, forKey: "spatialTimeToComplete")
        
        Results.setValue(theSumsRight, forKey: "sumsRight")
        Results.setValue(theSumsWrong, forKey: "sumsWrong")
        
        Results.setValue(strRepresentation1, forKey: "allSums")
        Results.setValue(strRepresentation2, forKey: "correctSumsAnswers")
        Results.setValue(strRepresentation3, forKey: "usersSumsAnswers")

        
        Results.setValue(didItSend, forKey: "sent")
        Results.setValue(date, forKey: "dateCompleted")
        
        print("saving a bit of the old core data here yo")
        
        clearAllTestData()
        //4
        var error: NSError?
        do {
            try managedContext.save()
        } catch let error1 as NSError {
            error = error1
            print("Could not save \(error), \(error?.userInfo)")
        }
        
    }
    ///////////////////////////
    //Save the data to core data
    ///////////////////////////

    
    
    ///////////////////////////////////////
    //Save the data to external database
    ///////////////////////////////////////
    func saveToDB() {
        
        let theQuestionaire = scoreObject.questionaire
        var data = NSData(bytes: theQuestionaire, length: theQuestionaire.count * sizeof(Float))
        
        let theReactions = scoreObject.reactionTimesResults
        let reactionsCorrect = scoreObject.correctReactions
        let reactionsEarly = scoreObject.earlyReactions
        var data2 = NSData(bytes: theReactions, length: theReactions.count * sizeof(String))
        
        let theSpatialComplete = scoreObject.spaticalNumberCompleted
        let theSpatialScore = scoreObject.spatialScore
        let theSpatialRight = scoreObject.spaticalNumberRight
        let theSpatialWrong = scoreObject.spaticalNumberWrong
        let theSpatialTimeToComplete = scoreObject.spaticalTimeToCompete
        
        let theSumsRight = scoreObject.sumsRight
        let theSumsWrong = scoreObject.sumsWrong
        let allSumsQuestions = scoreObject.allAdditionQuestions
        print(allSumsQuestions)

        let correctSumsAnswers = scoreObject.correctAdditionAnswers
        let userSumsAnswers = scoreObject.userAdditionAnswers
        
        let date = NSDate()
        let formatter: NSDateFormatter = NSDateFormatter()
        formatter.dateFormat = "yyyy-MM-dd-HH-mm-ss"
        let dateTimePrefix: String = formatter.stringFromDate(date)
    
        let uniqueID = UIDevice.currentDevice().identifierForVendor!.UUIDString
    
        let bodyData = "userId=\(uniqueID)&questionResults=\(theQuestionaire)&reactionResults=\(theReactions)&reactionsCorrect=\(reactionsCorrect)&reactionsEarly=\(reactionsEarly)&spatialComplete=\(theSpatialComplete)&spatialScore=\(theSpatialScore)&spatialRight=\(theSpatialRight)&spatialWrong=\(theSpatialWrong)&spatialToComplete=\(theSpatialTimeToComplete)&sumsRight=\(theSumsRight)&sumsWrong=\(theSumsWrong)&allSums=\(allSumsQuestions)&correctSums=\(correctSumsAnswers)&userSumsAnswers=\(userSumsAnswers)&date=\(dateTimePrefix)" //To get them in php: $_POST['name']
    
        let URL: NSURL = NSURL(string: "https://scm.ulster.ac.uk/~b00565433/service.php")!
        let request:NSMutableURLRequest = NSMutableURLRequest(URL:URL)
        request.HTTPMethod = "POST"
        request.HTTPBody = bodyData.dataUsingEncoding(NSUTF8StringEncoding);
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue())
            {
                (response, data, error) in
                if error != nil {
                    print("theres an error")
                    print(error)
                    print(response)
                    print(data)
                    self.didItSend = false
                    self.saveCoreData()
                    
                }
                else{
                    print("theres no error")
                    self.didItSend = true
                    self.saveCoreData()
                    self.checkCoreDB()
                    
                }
        }
    }
    ///////////////////////////////////////
    //Save the data to external database
    ///////////////////////////////////////
    
    func checkCoreDB(){
        //1
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        
        let managedContext = appDelegate.managedObjectContext!
        
        //2
        let fetchRequest = NSFetchRequest(entityName:"TestResultsData")
        fetchRequest.predicate = NSPredicate(format: "sent == %@", false)
        
        //3
        var error: NSError?
        
        let fetchedResults = try! managedContext.executeFetchRequest(fetchRequest) as! [NSManagedObject]
        
        //println(fetchedResults)
        
        useThis.results = fetchedResults
        
        
//        if let theData = fetchedResults {
//            useThis.results = theData
//        } else {
//            print("Could not fetch \(error), \(error!.userInfo)")
//        }
        
        //take all of the time data from the data in the database and put it into a array for calculations
        //var arrayForCalculations: Array<Double> = []
        
        for index in useThis.results{
            print("start my we loop")
            
             //the number of elements:
            var item = index.valueForKey("questionaire") as! NSData
            let count = item.length / sizeof(Float)

            // create array of appropriate length:
            var array = [Float](count: count, repeatedValue: 0)

            // copy bytes into array
            item.getBytes(&array, length:count * sizeof(Float))
            
            print(array)


            //the number of elements:
            var item1 = index.valueForKey("reactionTimesResults") as! NSData
            print(item1)
            let count1 = item1.length / sizeof(Float)
            
            // create array of appropriate length:
            var array1 = [Float](count: count1, repeatedValue: 0)
            
            // copy bytes into array
            item1.getBytes(&array1, length:count1 * sizeof(Float))
            
            print(array1)
            

            var item2 = index.valueForKey("dateCompleted") as! NSDate
            print(item2)

            var item3 = index.valueForKey("spatialNumberComplete") as! Int
            print(item3)

            var item4 = index.valueForKey("spatialScore") as! Int
            print(item4)

            var item5 = index.valueForKey("sumsRight") as! Int
            print(item5)

            var item6 = index.valueForKey("sumsWrong") as! Int
            print(item6)
            
            var allSums = index.valueForKey("allSums") as! String
            print(allSums)
            
            var correctSums = index.valueForKey("correctSumsAnswers") as! String
            print(correctSums)
            
            var userSums = index.valueForKey("usersSumsAnswers") as! String
            print(userSums)

            var item7 = index.valueForKey("sent") as! Bool
            print(item7)
            
            var item8 = index.valueForKey("spaticalNumberRight") as! Int
            
            var item9 = index.valueForKey("spaticalNumberWrong") as! Int
            
            var item10 = index.valueForKey("correctReactions") as! Int
            
            var item11 = index.valueForKey("earlyReactions") as! Int
            print(item11)
            
            //the number of elements:
            var item12 = index.valueForKey("spatialTimeToComplete") as! String
            print(item12)
            
//            let count12 = item12.length / sizeof(Float)
//            println(count12)
//            
//            // create array of appropriate length:
//            var array12 = [Float](count: count12, repeatedValue: 0)
//            println(array12)
//            
//            // copy bytes into array
//            item12.getBytes(&array12, length:count12 * sizeof(Float))
//            
//            println(array12)
            
            var theId = index
            
            saveFromCoreToDB(array, item1: array1, item2: item2, item3: item3, item4: item4, item5: item5, item6: item6, item7: item7, item8: item8, item9: item9, item10: item10, item11: item11,item12: item12, moid: theId, allSums: allSums, correctSums:correctSums, userSums:userSums)
        }

    }
    
    func saveFromCoreToDB(item:[Float], item1:[Float], item2:NSDate, item3:Int, item4:Int, item5:Int, item6:Int, item7:Bool, item8:Int, item9:Int, item10:Int, item11:Int, item12:String, moid:NSManagedObject, allSums:String, correctSums: String, userSums:String) {
        
        print("saeving time")
        
        let theQuestionaire = item
        
        let theReactions = item1
        let reactionsCorrect = item10
        let reactionsEarly = item11
        
        let date = item2
        
        let theSpatialComplete = item3
        let theSpatialScore = item4
        let theSpatialRight = item8
        let theSpatialWrong = item9
        let theSpatialTimeToComplete = item12
        
        let theSumsRight = item5
        let theSumsWrong = item6
        let allSumsQuestions = allSums
        print(allSumsQuestions)
        
        let correctSumsAnswers = correctSums
        let userSumsAnswers = userSums
        
        let formatter: NSDateFormatter = NSDateFormatter()
        formatter.dateFormat = "yyyy-MM-dd-HH-mm-ss"
        let dateTimePrefix: String = formatter.stringFromDate(date)
        
        let uniqueID = UIDevice.currentDevice().identifierForVendor!.UUIDString
        
        let bodyData = "userId=\(uniqueID)&questionResults=\(theQuestionaire)&reactionResults=\(theReactions)&reactionsCorrect=\(reactionsCorrect)&reactionsEarly=\(reactionsEarly)&spatialComplete=\(theSpatialComplete)&spatialScore=\(theSpatialScore)&spatialRight=\(theSpatialRight)&spatialWrong=\(theSpatialWrong)&spatialToComplete=\(theSpatialTimeToComplete)&sumsRight=\(theSumsRight)&sumsWrong=\(theSumsWrong)&allSums=\(allSumsQuestions)&correctSums=\(correctSumsAnswers)&userSumsAnswers=\(userSumsAnswers)&date=\(dateTimePrefix)" //To get them in php: $_POST['name']
        
        let URL: NSURL = NSURL(string: "https://scm.ulster.ac.uk/~b00565433/service.php")!
        let request:NSMutableURLRequest = NSMutableURLRequest(URL:URL)
        request.HTTPMethod = "POST"
        request.HTTPBody = bodyData.dataUsingEncoding(NSUTF8StringEncoding);
        NSURLConnection.sendAsynchronousRequest(request, queue: NSOperationQueue.mainQueue())
            {
                (response, data, error) in
                if error != nil {
                    print("theres an error")
                    
                }
                else{
                    print("theres no error")
                    
                    
                    //update the sent field to true if the data is sent to the server
                    let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                    
                    let managedContext = appDelegate.managedObjectContext!
                    
                    let batchRequest = NSBatchUpdateRequest(entityName: "TestResultsData")
                    batchRequest.predicate = NSPredicate(format: "self == %@", moid)
                    batchRequest.propertiesToUpdate = ["sent" : true]
                    batchRequest.resultType = .UpdatedObjectsCountResultType
                    var error : NSError?
                    var results = (try! managedContext.executeRequest(batchRequest)) as! NSBatchUpdateResult
                    if error == nil {
                        print("updated false to true")
                    }
                    else {
                        print("Update Message Error: \(error?.localizedDescription)")
                    }
                    
                }
        }
    }
    ///////////////////////////////////////
    //Save the data to external database
    ///////////////////////////////////////

    
    class coreDataNotSent {
        var results = [NSManagedObject]()
    }
    var useThis = coreDataNotSent()
    
    func clearAllTestData(){
        scoreObject.spatialScore = Int()
        scoreObject.spaticalNumberCompleted = Int()
        scoreObject.spaticalNumberRight = Int()
        scoreObject.spaticalNumberWrong = Int()
        scoreObject.spaticalTimeToCompete = [Float]()
        
        scoreObject.reactionTimesResults = [Float]()
        scoreObject.earlyReactions = Int()
        scoreObject.correctReactions = Int()
        
        scoreObject.sumsRight = Int()
        scoreObject.sumsWrong = Int()
        scoreObject.questionaire = [Int]()
        

    }


}