//
//  MonkeyViewController.swift
//  Reactions
//
//  Created by Edward Price on 28/01/2015.
//  Copyright (c) 2015 Edward Price. All rights reserved.
//

import UIKit

class MonkeyViewController: UIViewController {
    
    @IBOutlet weak var box1: UIButton!
    @IBOutlet weak var box2: UIButton!
    @IBOutlet weak var box3: UIButton!
    @IBOutlet weak var box4: UIButton!
    @IBOutlet weak var box5: UIButton!
    @IBOutlet weak var box6: UIButton!
    @IBOutlet weak var box7: UIButton!
    @IBOutlet weak var box8: UIButton!
    @IBOutlet weak var box9: UIButton!
    @IBOutlet weak var box10: UIButton!
    @IBOutlet weak var box11: UIButton!
    @IBOutlet weak var box12: UIButton!
    @IBOutlet weak var box13: UIButton!
    @IBOutlet weak var box14: UIButton!
    @IBOutlet weak var box15: UIButton!
    @IBOutlet weak var box16: UIButton!

    
    @IBOutlet weak var instructions: UILabel!
    @IBOutlet weak var thePauseButton: UIButton!
    
    //@IBOutlet weak var gameTimer: UILabel!
    
    @IBOutlet weak var gameTimer: UILabel!
    
    @IBOutlet weak var gameView: UIView!
    
    @IBOutlet weak var userOutcome: UIImageView!
    
    @IBOutlet weak var progressBar: UIProgressView!
    @IBOutlet weak var progressLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        /* load a count down view from timerView */
        self.view.userInteractionEnabled = false
        let gamelaunchTimerView:TimerView = TimerView.loadingCountDownTimerViewInView(self.view)
        gamelaunchTimerView.startTimer()
        
        var timer = NSTimer.scheduledTimerWithTimeInterval(4.2, target: self, selector: Selector("startGame"), userInfo: nil, repeats: false)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: ("backgroundTime"), name: "UIApplicationDidEnterBackgroundNotification", object: nil)

    }
    
    func backgroundTime(){
        
        isPaused = true
        
        resetColors()
        
        //pause the game timer
        theGameTimer.invalidate()
        
        //stop the user input starting
        userInputTimer.invalidate()
        
        //invalidate the "too slow" timer for the user and empty the sequence
        userTimer.invalidate()
        genSequence = []
        
        //reset this counter so that things fire again
        displaySequenceComplete = 0
        
        //stops the rest of the sequence showing
        showSequenceTimer.invalidate()
        
        
        //just another timer that i now have to invalidate
        delayBeforeShow.invalidate()
        
        self.gameView.userInteractionEnabled = false
        createPauseView()
    }
    
    var difficulty = 3              //user difficulty, number of flashing squares
    var level = 1                   //current level
    var score = 0                   //current score which is the highest level reached
    var numberCompleted = 0
    var displaySequenceComplete = 0 //number of display sequence shown
    var turn = 0                    //current turn
    var active = false              //whether a turn is active or not
    var genSequence = [Int]()       //array containing the generated/randomized pads
    var interval = 1.7              //length of time a squaer flashes
    var timerIsRunning = false
    
    var numberRight = 0
    var numberWrong = 0
    
    //game timer variables
    var theGameTimer = NSTimer()
    var timerVal = 90
    
    var isPaused = false

    
    func startGame(){
        self.thePauseButton.enabled = true
        self.gameView.userInteractionEnabled = false
        self.view.userInteractionEnabled = true
        set_up()
        turn = 0
        instructions.text = "Remember this "+String(difficulty)+" square pattern"
        
        resetPrgress()
        
        if timerIsRunning == false {
            startTimer()
            timerIsRunning = true
        }
    }

    
    func startTimer()
    {
        theGameTimer = NSTimer.scheduledTimerWithTimeInterval(1.0, target: self, selector: Selector("updateTimer:"), userInfo: nil, repeats: true)
    }
    
    func updateTimer(dt:NSTimer)
    {
        timerVal--
        if timerVal==0{
            scoreObject.spatialScore = difficulty
            scoreObject.spaticalNumberCompleted = numberCompleted
            scoreObject.spaticalNumberRight = numberRight
            scoreObject.spaticalNumberWrong = numberWrong
            //pause the game timer
            theGameTimer.invalidate()
            
            //stop the user input starting
            userInputTimer.invalidate()
            
            //invalidate the "too slow" timer for the user and emoty the sequence
            userTimer.invalidate()
            genSequence = []
            
            //reset this counter so that things fire again
            displaySequenceComplete = 0
            
            //stops the rest of the sequence showing
            showSequenceTimer.invalidate()
            
            //just another timer that i now have to invalidate
            delayBeforeShow.invalidate()
            
            performSegueWithIdentifier("SpatialSegue", sender: nil)
            
        }else if timerVal<0{
            theGameTimer.invalidate()
        } else{
            UIView.setAnimationsEnabled(false)
            gameTimer.text = "\(timerVal)"
            UIView.setAnimationsEnabled(true)
        }
    }
    
    func removeCountDownTimerView()
    {
        timerVal = 90
        
    }

    
    // Set up the game
    func set_up() {
        // create the sequence:
        for (var i=0; i<difficulty; i++) {
            genSequence.append(Int((arc4random_uniform(16)) + 1))
        }
        start_round()
    }
    
    var showSequenceTimer = NSTimer()
    
    // Show the sequence
    func start_round() {
        
        for (var i=0; i<genSequence.count; i++) {
                
            print("start round gen sequence")
            let time = Double(i)*interval
                
            showSequenceTimer = NSTimer.scheduledTimerWithTimeInterval(time, target: self, selector:"correctData:", userInfo: genSequence[i], repeats: false)
        }
    }
    
    var delayBeforeShow = NSTimer()
    
    func correctData(timer: NSTimer) {

        if genSequence.count < 2 {print("no printings for you")}
        else{
            let value = timer.userInfo as! Int
            resetColors()
            
            //this delay is the time between each flash
            delayBeforeShow = NSTimer.scheduledTimerWithTimeInterval(0.3, target: self, selector:"correctDelayData:", userInfo: value, repeats: false)
            
        }
    }
    
    func correctDelayData(theval: NSTimer){
        if isPaused == true {
            delayBeforeShow.invalidate()
        }
        
        else{
            let value = theval.userInfo as! Int
            print("\(value) correct data2")
            flash_quarter(value)
        }
    
    }

    
    var userInputTimer = NSTimer()
    
    func flash_quarter(n: Int) {
    
        if isPaused == true {
        }
        
        else{
            
            print("\(n) flash quarter")
            for (var i=0; i<=16; i++) {
                if (i == n) {
                    let theButton : UIButton? = self.view.viewWithTag(n) as? UIButton;
                    theButton?.backgroundColor = UIColor(red: 80/255, green: 210/255, blue: 194/255, alpha: 1)
                    displaySequenceComplete++
                }
            }
            
            print(displaySequenceComplete)
            print(genSequence.count)
            print(genSequence)
        
            if displaySequenceComplete == genSequence.count{
                var timer = NSTimer.scheduledTimerWithTimeInterval((interval - 0.4), target: self, selector:"resetColors", userInfo: nil, repeats: false)
                userInputTimer = NSTimer.scheduledTimerWithTimeInterval((interval - 0.4), target: self, selector:"startUserInput", userInfo: nil, repeats: false)
                displaySequenceComplete = 0
            }
        }
    }
    
    func resetColors(){
        for (var i=0; i<=25; i++) {
            let theButton : UIButton? = self.view.viewWithTag(i) as? UIButton;
            theButton?.backgroundColor = UIColor(red: 244/255, green: 244/255, blue: 244/255, alpha: 1)
        }
    }
    
    func updatePrgress(){
        let theTurn = Double(turn + 1)
        let diff = Double(difficulty)
        let barPercent = Float( theTurn / diff )
        print(barPercent)
        progressBar.progress = barPercent
        //progressLabel.text = "Sequence Complete \(turn + 1)/\(difficulty)"
    }
    
    func resetPrgress(){
        progressBar.progress = 0
        //progressLabel.text = "Sequence Complete 0/\(genSequence.count)"
    }
    
    var userTimer = NSTimer()

    var startTime = NSTimeInterval()
    
    func startUserInput(){
        startTime = NSDate.timeIntervalSinceReferenceDate()
        self.view.userInteractionEnabled = true
        self.gameView.userInteractionEnabled = true
        
        instructions.text = "Enter the "+String(difficulty)+" square pattern"
        displayGo()

        userTimer = NSTimer.scheduledTimerWithTimeInterval(5, target: self, selector: Selector("tooSlow"), userInfo: nil, repeats: false)
    }
    
    func tooSlow(){
        genSequence = []
        userTimer.invalidate()
        instructions.text = "Too slow, please try again"
        displayWrong()
        
        resetPrgress()
        var timer = NSTimer.scheduledTimerWithTimeInterval(1.5, target: self, selector:"startGame", userInfo: nil, repeats: false)
    }
    
    @IBAction func sequenceButton(sender: AnyObject) {
        let reference = sender.tag
        let theButton : UIButton? = self.view.viewWithTag(reference) as? UIButton;
        theButton?.backgroundColor = UIColor(red: 244/255, green: 244/255, blue: 244/255, alpha: 1)
        
        userTimer.invalidate()
        userTimer = NSTimer.scheduledTimerWithTimeInterval(5, target: self, selector: Selector("tooSlow"), userInfo: nil, repeats: false)
        userTry(reference)
    }
    
    @IBAction func sequenceButtonTouchDown(sender: AnyObject) {
        let reference = sender.tag
        let theButton : UIButton? = self.view.viewWithTag(reference) as? UIButton;
        theButton?.backgroundColor = UIColor(red: 80/255, green: 210/255, blue: 194/255, alpha: 1)
    }
    
    func userTry(n:Int){
        if genSequence.count == 0 {}
        else{
            if n == genSequence[turn]{
                            
                if turn == difficulty-1{
                    let currentTime = NSDate.timeIntervalSinceReferenceDate()
                    //Find the difference between current time and start time.
                    let elapsedTime: NSTimeInterval = currentTime - startTime
                    
                    let formattedTime = String(format:"%.2f", elapsedTime)
                    let formatConversion = (formattedTime as NSString).floatValue
                    
                    scoreObject.spaticalTimeToCompete.append(formatConversion)
                    
                    updatePrgress()
                    score = difficulty
                    difficulty++
                    level++
                    numberCompleted++
                    genSequence = []
                    
                    displayRight()
                    numberRight++
                    var timer = NSTimer.scheduledTimerWithTimeInterval(1.5, target: self, selector:"startGame", userInfo: nil, repeats: false)
                }
                else{
                    updatePrgress()
                    turn++
                }
            }
            
            else{
                genSequence = []
                
                if difficulty >= 4{
                    difficulty--
                }
                
                displayWrong()
                numberWrong++
                var timer = NSTimer.scheduledTimerWithTimeInterval(1.5, target: self, selector:"startGame", userInfo: nil, repeats: false)
            }
        }
        
    }
    
    func displayWrong() {
        self.view.userInteractionEnabled = false
        userOutcome.image = UIImage(named: "red-ex")
        userOutcome.alpha = 1
        userOutcome.fadeOut()
        userTimer.invalidate()
    }
    
    func displayRight() {
        self.view.userInteractionEnabled = false
        userOutcome.image = UIImage(named: "green-tick")
        userOutcome.alpha = 1
        userOutcome.fadeOut()
        userTimer.invalidate()
    }
    
    func displayGo() {
        userOutcome.image = UIImage(named: "go")
        userOutcome.alpha = 1
        userOutcome.fadeOut()
        userTimer.invalidate()
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    /*START OF PAUSE FUNCTIONALITY*/
    
    var pauseView = PauseView()
    
    @IBAction func pauseBtn(sender: AnyObject) {
        
        isPaused = true
        
        resetColors()
        
        //pause the game timer
        theGameTimer.invalidate()
        
        //stop the user input starting
        userInputTimer.invalidate()
        
        //invalidate the "too slow" timer for the user and emoty the sequence
        userTimer.invalidate()
        genSequence = []
        
        //reset this counter so that things fire again
        displaySequenceComplete = 0
        
        //stops the rest of the sequence showing
        showSequenceTimer.invalidate()

        
        //just another timer that i now have to invalidate
        delayBeforeShow.invalidate()
        
        self.gameView.userInteractionEnabled = false
        createPauseView()
        
    }
    
    var myFirstButton = UIButton()
    
    func createPauseView(){
        
        let superView = self.view
        

        /* create new pause view */
        pauseView = PauseView(frame:superView.frame)
        pauseView.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(1)
        pauseView.layer.zPosition = 1;
        superView.addSubview(pauseView)
        
        /* add a custom Circle view */
        
        let refFrame:CGRect = CGRectMake(superView.center.x-50, superView.center.y-50, 2*50, 2*50)
        let circleView:UIView = UIView(frame:refFrame)
        circleView.layer.cornerRadius = 50
        circleView.layer.borderColor = UIColor.whiteColor().CGColor
        circleView.layer.borderWidth = 2
        
        let myFirstLabel = UILabel()
        myFirstLabel.text = "WARNING! Sequence will reset and restart when you press play"
        myFirstLabel.textColor = UIColor.whiteColor()
        myFirstLabel.textAlignment = .Center
        myFirstLabel.numberOfLines = 5
        myFirstLabel.frame = CGRectMake(superView.center.x-150, superView.center.y+30, 300, 200)
        
        /* add a custom Button  */
        myFirstButton.frame = circleView.bounds
        myFirstButton.userInteractionEnabled = false
        myFirstButton.alpha = 0
        var allowInputTimer = NSTimer.scheduledTimerWithTimeInterval(2, target: self, selector:"enablePlayInput", userInfo: nil, repeats: false)
        myFirstButton.setTitle("Play", forState: .Normal)
        myFirstButton.setTitleColor(UIColor.whiteColor(), forState: .Normal)
        myFirstButton.addTarget(self, action: "pressed:", forControlEvents: UIControlEvents.TouchUpInside)
        circleView.addSubview(myFirstButton)
        
        pauseView.addSubview(circleView)
        pauseView.addSubview(myFirstLabel)
        
        superView.addSubview(pauseView)
        
        myFirstButton.fadeIn()
        
    }
    
    func enablePlayInput(){
        print("working ok")
        myFirstButton.userInteractionEnabled = true
    }
    
    func pressed(sender:UIButton!){
        
        // set is paused variable to false
        isPaused = false
        //reset the colours to grey
        resetColors()
        
        var mySuperView:UIView = self.view
        pauseView.removeFromSuperview()
        self.view.userInteractionEnabled = true
        self.gameView.userInteractionEnabled = true
        theGameTimer = NSTimer.scheduledTimerWithTimeInterval(1.0 , target: self, selector: Selector("updateTimer:"), userInfo: nil, repeats: true)
        genSequence = []
        displaySequenceComplete = 0
        startGame()
    }
    /*END OF PAUSE FUNCTIONALITY*/
    
    

}


