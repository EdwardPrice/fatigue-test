//
//  TimerView.swift
//  Reactions
//
//  Created by Edward Price on 10/02/2015.
//  Copyright (c) 2015 Edward Price. All rights reserved.
//

import Foundation
import UIKit
import QuartzCore

let VIEW_ALPHA:CGFloat = 0.5
let TIMERVIEW_RADIUS:CGFloat = 50
var TIMER_LABEL_INITIAL_VAL:Int =  3
let BORDER_WIDTH:CGFloat = 2
var timerVal:Int = TIMER_LABEL_INITIAL_VAL;
var timer:NSTimer!



class TimerView :UIView {
    
    struct Stored {
        static var timerLbl:UILabel!
    }
    
    
    class func loadingCountDownTimerViewInView (_superView:UIView)-> TimerView
    {
        let timerView:TimerView = TimerView(frame:_superView.frame)
        timerView.backgroundColor = UIColor.blackColor().colorWithAlphaComponent(VIEW_ALPHA)
        timerView.layer.zPosition = 1;
        _superView.addSubview(timerView)
        
        /* add a custom Circle view */
        
        let refFrame:CGRect = CGRectMake(_superView.center.x-TIMERVIEW_RADIUS, _superView.center.y-TIMERVIEW_RADIUS, 2*TIMERVIEW_RADIUS, 2*TIMERVIEW_RADIUS)
        let circleView:UIView = UIView(frame:refFrame)
        circleView.layer.cornerRadius = TIMERVIEW_RADIUS
        circleView.layer.borderColor = UIColor.whiteColor().CGColor
        circleView.layer.borderWidth = BORDER_WIDTH
        
        /* add a custom Label */
        
        Stored.timerLbl = UILabel(frame:circleView.bounds)
        Stored.timerLbl.text = "\(TIMER_LABEL_INITIAL_VAL)"
        Stored.timerLbl.textColor = UIColor.whiteColor()
        Stored.timerLbl.font = UIFont(name: "MarkerFelt-Thin", size: 40)
        Stored.timerLbl.textAlignment = NSTextAlignment.Center
        
        circleView.addSubview(Stored.timerLbl)
        timerView.addSubview(circleView)
        
        return timerView
    }
    
    func startTimer()
    {
        timer = NSTimer.scheduledTimerWithTimeInterval(1.0
            , target: self, selector: Selector("updateTimer:"), userInfo: nil, repeats: true)
    }
    
    func updateTimer(dt:NSTimer)
    {
        timerVal--
        if timerVal==0{
            Stored.timerLbl.text = "Go"
        }else if timerVal<0{
            timer.invalidate()
            removeCountDownTimerView()
        } else{
            Stored.timerLbl.text = "\(timerVal)"
        }
    }
    
    func removeCountDownTimerView()
    {
        let mySuperView:UIView = self.superview!
        mySuperView.userInteractionEnabled = true
        super.removeFromSuperview()
        timerVal = 3
        TIMER_LABEL_INITIAL_VAL = 3

    }
}