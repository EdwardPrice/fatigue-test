//
//  QuestionOne.swift
//  Reactions
//
//  Created by Edward Price on 09/03/2015.
//  Copyright (c) 2015 Edward Price. All rights reserved.
//

import Foundation
import UIKit

class QuestionOne: UIViewController, UITableViewDelegate, UITableViewDataSource  {
    

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var question: UILabel!
    
    //let textCellIdentifier = "TextCell"
    
    var currentQuestion = 1
    
    
    var answers = ["1. I do not feel fatigued at all (No abnormal fatigue).", "2.", "3. I feel fatigued several times every day, but I feel more alert after rest.", "4.", "5. I feel fatigued for most of the day and taking a rest has little or no effect.", "6.", "7. I feel fatigued all the time and taking a rest makes no difference."]
    
    var isChecked = false
    
    // MARK:  UITextFieldDelegate Methods
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return answers.count
    }
    

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("setupCell", forIndexPath: indexPath) 
        
//        tableView.contentInset = UIEdgeInsetsZero;
//        tableView.separatorInset = UIEdgeInsetsZero
//        tableView.preservesSuperviewLayoutMargins = false
//        tableView.layoutMargins = UIEdgeInsetsZero
        
        let row = indexPath.row
        
        
        if let answerLabel = cell.viewWithTag(100) as? UILabel { //3
            answerLabel.text = answers[row]
        }
        if let imageLabel = cell.viewWithTag(101) as? UIImageView {
            imageLabel.image = UIImage(named: "unchecked")
            //imageLabel.frame = CGRectMake( 0, 0, 55, 55 )
        }
        
        //cell.textLabel?.text = answers[row]
        //cell.textLabel?.font = UIFont.systemFontOfSize(13.0)
        //cell.textLabel?.sizeToFit()
        //cell.textLabel?.lineBreakMode = .ByWordWrapping // or NSLineBreakMode.ByWordWrapping
        //cell.textLabel?.numberOfLines = 0
        
        cell.separatorInset = UIEdgeInsetsZero
        cell.preservesSuperviewLayoutMargins = false
        cell.layoutMargins = UIEdgeInsetsZero
        
        //cell.imageView?.frame = CGRectMake( 0, 0, 55, 55 )
        //cell.imageView?.image = UIImage(named: "unchecked")

        
        return cell
    }
    
    // MARK:  UITableViewDelegate Methods
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        // Iterate over all the rows of a section
        for (var row = 0; row < tableView.numberOfRowsInSection(0); row++) {
            let cell = tableView.cellForRowAtIndexPath(NSIndexPath(forRow: row, inSection: 0))
            
            if let imageLabel = cell!.viewWithTag(101) as? UIImageView {
                imageLabel.image = UIImage(named: "unchecked")
            }
            //cell?.imageView?.image = UIImage(named: "unchecked")
            cell?.tag = 0
            // do something with the cell here.
        }
        
        let currentCell = tableView.cellForRowAtIndexPath(indexPath)
        
        if let imageLabel = currentCell!.viewWithTag(101) as? UIImageView {
            imageLabel.image = UIImage(named: "checked")
            //imageLabel.frame = CGRectMake( 0, 0, 55, 55 )
        }
        //currentCell!.imageView?.image = UIImage(named: "checked")
        currentCell!.tag = 1
        
        isChecked = true
        
    }
    
    
    @IBAction func submit(sender: AnyObject) {
        
        if isChecked == false {
            let alertController = UIAlertController(title: "", message:"Please choose an option", preferredStyle: UIAlertControllerStyle.Alert)
            alertController.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default,handler: nil))
            
            self.presentViewController(alertController, animated: true, completion: nil)
        }
        
        else {
            isChecked = false
            currentQuestion++
            print(currentQuestion)
            
            // Iterate over all the rows
            for (var row = 0; row < tableView.numberOfRowsInSection(0); row++) {
                let cell = tableView.cellForRowAtIndexPath(NSIndexPath(forRow: row, inSection: 0))
                
                // if the cell is selected eg has the tag of one
                if cell?.tag == 1 {
                    let selectedAnswer = row + 1
                    print(selectedAnswer)
                    scoreObject.questionaire.append(selectedAnswer)
                    //performSegueWithIdentifier("Q1Segue", sender: nil)
                }
                
                //reset the cell to unchecked and the tag to 0
                if let imageLabel = cell!.viewWithTag(101) as? UIImageView {
                    imageLabel.image = UIImage(named: "unchecked")
                    imageLabel.frame = CGRectMake( 0, 0, 50, 50 )
                }
                //cell?.imageView?.image = UIImage(named: "unchecked")
                cell?.tag = 0
            }
            
            //
            //change this to either have all questions or go to the next section!!!!!!!!!!!!
            //
            nextQuestion()
            //performSegueWithIdentifier("Q1Segue", sender: nil)
        
        }
        
    }
    
    func changeQuestion(theQuestion: String){
        UIView.animateWithDuration(0.2, delay: 0.0, options: UIViewAnimationOptions.CurveEaseOut, animations: {
            self.question.alpha = 0.0
            }, completion: {
                (finished: Bool) -> Void in
                
                //Once the label is completely invisible, set the text and fade it back in
                self.question.text = theQuestion
                
                // Fade in
                UIView.animateWithDuration(0.2, delay: 0.0, options: UIViewAnimationOptions.CurveEaseIn, animations: {
                    self.question.alpha = 1.0
                    }, completion: nil)
        })
    }
    
    func nextQuestion(){
    
        switch currentQuestion {
        case 1:
            print("still first question")
        case 2:
            changeQuestion("Q2. Do you find it difficult to start things? Do you experience resistance or a lack of initiative when you have to start something, no matter whether it is a new task or part of your everyday activities?")

            answers = []
            answers = ["1. I have no difficulty starting things.", "2.", "3. I find it more difficult starting things than I used to. I’d rather do it some other time.", "4.", "5. It takes a great effort to start things. This applies to everyday activities such as getting out of bed, washing and eating.", "6.", "7. I can’t do the simplest of everyday tasks (eating, getting dressed). I need help with everything."]

        case 3:
            changeQuestion("Q3. Does your brain become fatigued quickly when you have to think hard? Do you feel mentally fatigued from things such as reading, watching TV or talking with several people?")
            answers = []
            answers = ["1. I can manage in the same way as usual. My ability for sustained mental effort is not reduced.", "2.", "3. I become fatigued quickly but am still able to make the same mental effort as before.", "4.", "5. I become fatigued quickly and have to take a break or do something else more often than before.", "6.", "7. I become fatigued so quickly that I can do nothing or have to abandon everything after a short period (5 mins)."]

        case 4:
            changeQuestion("Q4. If you have to take a break, how long do you need to recover after you have worked “until you drop” or are no longer able to concentrate on what you are doing?")
            answers = []
            answers = ["1. I need to rest for less than an hour before continuing whatever I am doing.", "2.", "3. I need to rest for more than an hour but do not require a night’s sleep.", "4.", "5. I need a night’s sleep before I can continue whatever I am doing.", "6.", "7. I need several days rest in order to recover."]
        case 5:
            changeQuestion("Q5. Do you find it difficult to gather your thoughts and concentrate?")
            answers = []
            answers = ["1. I can concentrate as usual.", "2.", "3. I sometimes lose concentration, for example when reading or watching TV.", "4.", "5. I find it so difficult to concentrate that I have problems, for example reading or taking part in a conversation with people.", "6.", "7. I always have such difficulty concentrating that it is almost impossible to do anything."]
        case 6:
            changeQuestion("Q6. Do you forget things more often than before, do you need to make notes or do you have to search for things at home or at work?")
            answers = []
            answers = ["1. I have no memory problems.", "2.", "3. I forget things slightly more often than I should, but I am able to manage by making notes.", "4.", "5. My poor memory causes frequent problems, for example forgetting meetings or to turn off the cooker.", "6.", "7. I can hardly remember anything at all."]
        case 7:
            changeQuestion("Q7. Do you feel slow or sluggish when you think about something? Do you feel that it takes an unusually long time to conclude a train of thought or solve a task that requires mental effort?")
            answers = []
            answers = ["1. My thoughts are neither slow nor sluggish when it comes to work involving mental effort.", "2.", "3. My thoughts are a bit slow a few times each day when I have to do something that requires serious mental effort.", "4.", "5. My thoughts often feel slow and sluggish, even when carrying out everyday activities, such as reading.", "6.", "7. My thoughts always feel very slow and sluggish."]
        case 8:
            changeQuestion("Q8. Do you find it difficult to cope with stress that is, doing several things at the same time while under time pressure?")
            answers = []
            answers = ["1. I am able to cope with stress, in the same way as usual.", "2.", "3. I become more easily stressed, but only in demanding situations that I was previously able to manage.", "4.", "5. I become stressed more easily than before. I feel stressed in situations that previously did not bother me.", "6.", "7. I become stressed very easily. I feel stressed in unfamiliar or trying situations."]
        case 9:
            changeQuestion("Q9. Do you find that you cry more easily than normally? For example you burst into tears when you watch a sad film or talk with family members? If you recently experienced an accident or short illness try to disregard it.")
            answers = []
            answers = ["1. I am not more emotional than I used to be.", "2.", "3. I am more emotional than other people but it is natural for me. I start to cry or my eyes fill with tears easily.", "4.", "5. My emotions are problematic or embarrassing. I sometimes even start to cry about things that mean nothing to me.", "6.", "7. My emotions cause me great problems. They disturb my day-to-day relationship with members of my immediate family."]
        case 10:
            changeQuestion("Q10. Are you unusually short-tempered or irritable about things that previously did not bother you?")
            answers = []
            answers = ["1. I am not more short-tempered or irritable than I used to be.", "2.", "3. I become more easily irritated, but it does not last very long.", "4.", "5. I become irritated very quickly about small things or things that do not bother other people.", "6.", "7. I react with extreme anger or rage, which I find very difficult to control."]
        case 11:
            changeQuestion("Q11. Are you sensitive to strong light?")
            answers = []
            answers = ["1. I have no increased sensitivity to light.", "2.", "3. I sometimes experience problems with strong light or lights at home, but I am able to cope with it.", "4.", "5. I am so sensitive to light that I prefer to carry out activities in dim light.", "6.", "7. My sensitivity to light is so strong that I am unable to leave the house without sunglasses."]
        case 12:
            changeQuestion("Q12. Are you sensitive to noise?")
            answers = []
            answers = ["1. I do not suffer from increased sensitivity to noise.", "2.", "3. I sometimes have difficulty with loud noise, for example music or noise from the TV, but i can deal with it easily.", "4.", "5. I have a marked over-sensitivity to noise. I have to avoid loud noise or reduce it in order to cope.", "6.", "7. My sensitivity to noise is so great that I find it difficult to manage at home despite sound insulation."]
        case 13:
            changeQuestion("Q13. Do you sleep badly at night? If you are sleeping more than before at night, please choose the first option. If you are taking sleeping tablets and sleep normally, please choose the first option.")
            answers = []
            answers = ["1. I do not sleep less than before.", "2.", "3. I have slight problems falling asleep or my sleep is shorter, lighter or more restless than before.", "4.", "5. I sleep at least two hours less than before and wake up frequently during the night without anything disturbing me.", "6.", "7. I sleep less than two to three hours per night."]
        case 14:
            changeQuestion("Q14. Do you sleep longer and/or more deeply than before? If you are sleeping less than before, please choose the first option. Please take account of time spent sleeping during the day.")
            answers = []
            answers = ["1. I do not sleep more than usual", "2.", "3. I sleep longer or deeper, but less than two hours more than usual, including naps during the day.", "4.", "5. I sleep longer or deeper. At least two hours more than usual, including naps.", "6.", "7. I sleep longer or deeper. At least four hours more than usual, and in addition I need to take a nap during the day."]
        default:
            performSegueWithIdentifier("Q1Segue", sender: nil)
        }
        
        self.tableView.reloadSections(NSIndexSet(index: 0), withRowAnimation: UITableViewRowAnimation.Left)
        
    }

    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        question.text = "Q1. Fatigue. On a scale of 1 to 7, have you felt fatigued during the past day? It does not matter if the fatigue is physical (muscular) or mental. "
        
        
        tableView.delegate = self
        tableView.dataSource = self
        
        //self.tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "cell")
        self.tableView.rowHeight = 50.0
        
    }
    
    
}